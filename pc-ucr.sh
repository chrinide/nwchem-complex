#!/bin/bash
# Using ONLY internal BLAS ----- START
export USE_INTERNALBLAS=No
# Using ONLY internal BLAS ----- END
# Using openmpi 1.4.3 (which worked on my laptop)
#export USE_SCALAPACK=yes
#export SCALAPACK="-L/opt/intel/mkl/lib/intel64 -lmkl_scalapack_ilp64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lmkl_blacs_intelmpi_ilp64 -lpthread -lm"
export USE_LAPACK=yes
#export LAPACK="/usr/lib/x86_64-linux-gnu/liblapack.a"
#export LAPACK_SIZE=8
export LAPACK="-L /opt/intel/mkl/lib/intel64 -lmkl_lapack95_ilp64 -lmkl_blas95_ilp64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential  -lpthread -lm"

### Ravindra : Provide the location where the openmpi is installed
export NWCHEM_TOP=$PWD
export MPI_LOC=/usr/lib/x86_64-linux-gnu/openmpi
#export MPI_LOC=/home/fa/openmpi-1.4.3/my_lib
export MKL=/opt/intel/mkl/lib/intel64
export HAS_BLAS=yes
export BLAS_SIZE=8
export BLASOPT="/opt/intel/mkl/lib/intel64/libmkl_blas95_ilp64.a /opt/intel/mkl/lib/intel64/libmkl_lapack95_ilp64.a -L/opt/intel/mkl/lib/intel64 -lmkl_lapack95_ilp64 -lmkl_blas95_ilp64 -lmkl_intel_ilp64 -lmkl_intel_thread  -lmkl_core -liomp5 -lpthread -lm -ldl"

#export FOPTIMIZE=”-O3 -xSSE2,SSE3,SSSE3,SSE4.1,SSE4.2 -no-prec-div -funroll-loops -unroll-aggressive”
#export COPTIMIZE=”-O3 -xSSE2,SSE3,SSSE3,SSE4.1,SSE4.2 -no-prec-div -funroll-loops”

export LARGE_FILES=TRUE
export NWCHEM_TARGET=LINUX64

export PYTHONHOME=/usr/bin
export PYTHONVERSION=2.7
export USE_PYTHON64=y
export ENABLE_COMPONENT=yes
export USE_MPI=y
export USE_MPIF=y
export USE_MPIF4=y    # fix for pspw issue and 4-byte integers in MPI fortran bi

#export MPI_LIB=$MPI_LOC/lib64
export MPI_LIB=$MPI_LOC/lib
export MPI_INCLUDE=$MPI_LOC/include
#export LIBMPI="--enable-new-dtags -L/usr/mpi/intel/openmpi-1.4.3/lib64 -lmpi -lmpigf -lmpigi -lpthread -lrt"
export FC=gfortran
export CC=cc
export MPI_F90=mpif90
export MPI_CC=mpicc
export MPI_CXX=mpicxx
#export LIBMPI="--enable-new-dtags -L/usr/mpi/intel/openmpi-1.4.3/lib64 -lmpi -lopen-rte -lopen-pal -ldl -lnsl -lutil"

#export LIBMPI="-lmpi_f90 -lmpi_f77 -lmpi –lpthread -ldl -Wl,--export-dynamic -lnsl -lutil" 

#export LIBMPI="-lmpi_f90 -lmpi_f77 -lmpi -lpthread -libverbs -libumad -lrt -ldl -Wl,--export-dynamic -lnsl -lutil" 
# Fixing on 11-24-13 compilation:
#export LIBMPI="-lmpi_f90 -lmpi_f77 -lmpi -lpthread -lrt -ldl -Wl,--export-dynamic -lnsl -lutil" 
export LIBMPI="-L/usr/lib/x86_64-linux-gnu/openmpi -lmpi -lopen-pal -lopen-rte -lmpi_mpifh -lmpi_usempif08 -lmpi_usempi_ignore_tkr -lpthread -lrt -ldl -Wl,--export-dynamic -lnsl -lutil" 
#export LIBMPI="-L/home/fa/openmpi-1.4.3/my_lib/lib -lmpi_f90 -lmpi_f77 -lmpi -lpthread -lrt -ldl -Wl,--export-dynamic -lnsl -lutil" 


# determine where this script is located in the file system, and
# assign NWCHEM_TOP based on that
script_dir="$(dirname "$(readlink -f ${BASH_SOURCE[0]})")"
export NWCHEM_TOP=$script_dir

echo "NWCHEM_TOP=$NWCHEM_TOP"
echo "MPI_LOC=$MPI_LOC"

echo "You might want to change MAX_NPROC in src/tools/global/src/config.h"

echo "You need to do the following to build:"
echo "> cd $NWCHEM_TOP/src"
echo "> make nwchem_config NWCHEM_MODULES=shortqm"
echo "> make CC=$CC FC=$FC" 

export NWCHEM_EXEC=$NWCHEM_TOP/bin/$NWCHEM_TARGET/nwchem
export NWCHEM_EXECUTABLE=$NWCHEM_TOP/bin/$NWCHEM_TARGET/nwchem
export PATH=$NWCHEM_TOP/bin/$NWCHEM_TARGET:$PATH
#export NWCHEM_MODULES=smallqm
export NWCHEM_MODULES=qmandpw
export NWCHEM_BASIS_LIBRARY=$NWCHEM_TOP/src/basis/libraries/

#export ARMCI_NETWORK=OPENIB
#export MSG_COMMS=MPI
#export IB_INCLUDE=/usr/include/infiniband
#export IB_INCLUDE=/usr/src/kernels/2.6.18-348.6.1.el5-x86_64/include/config/infiniband
#export IB_HOME=/usr
#export IB_LIB=/usr/lib64
#export IB_LIB_NAME="-libumad -libverbs -lp-thread"
#export IB_LIB_NAME="-libumad -libverbs -lpthread"


# set scratch dir
if test -z $PBS_ENVIRONMENT
then
 export SCRATCH_DIR=$PBSTMPDIR
else
 export SCRATCH_DIR=/scratch
fi

